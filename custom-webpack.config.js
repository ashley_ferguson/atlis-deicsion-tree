
module.exports = {
  "module": {
    "rules": [
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!(dmn-js|dmn-js-drd|dmn-js-shared|dmn-js-decision-table|table-js|dmn-js-literal-expression|diagram-js)\/).*/,
        loader: 'babel-loader', query: {presets: ["@babel/preset-react", "@babel/preset-env"]}

      }]
  }
}
