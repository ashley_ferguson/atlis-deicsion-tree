import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-dmn-tab',
  templateUrl: './dmn-tab.component.html',
  styleUrls: ['./dmn-tab.component.scss']
})
export class DmnTabComponent implements OnInit {

  constructor() { }
  activeTabId: any;
  CLASS_NAMES: any

  ngOnInit(): void {
    this.activeTabId = 0;

    this.CLASS_NAMES = {
      drd: 'dmn-icon-lasso-tool',
      decisionTable: 'dmn-icon-decision-table',
      literalExpression: 'dmn-icon-literal-expression'
    };
  }
  @Input() views: [];
  @Output() tabClick = new EventEmitter<any>();

  getClassName(aView: any) {
    return this.CLASS_NAMES[aView.type];
  }

  onTabClick(aView: any, tabIndex: number) {
    this.tabClick.emit(aView);
    this.activeTabId = tabIndex;
  }

}
