import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmnTabComponent } from './dmn-tab.component';

describe('DmnTabComponent', () => {
  let component: DmnTabComponent;
  let fixture: ComponentFixture<DmnTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmnTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmnTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
