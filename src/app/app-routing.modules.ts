import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import { MsalGuard } from '@azure/msal-angular';
import {DmnEditorComponent} from './dmn-editor/dmn-editor.component';


const routes: Routes = [
  {path: '', canActivate: [ MsalGuard ], children: [
      { path: '', redirectTo: 'app', pathMatch: 'full' },
      { path: 'app',  component: AppComponent,
        children: [{
          path: 'decisions', component: DmnEditorComponent
        }]},
      { path: '**', redirectTo: 'app'}
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
