import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appDmnTabsWrapper]'
})
export class DmnTabsWrapperDirective {

  constructor(public viewContainerRef: ViewContainerRef) {}

}
