import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import { DmnEditorComponent } from './dmn-editor/dmn-editor.component';
import { DmnTabsWrapperDirective } from './dmn-tabs-wrapper.directive';
import { DmnTabComponent } from './dmn-tab/dmn-tab.component';
import {MsalModule, MsalInterceptor} from '@azure/msal-angular';

const isIE = window.navigator.userAgent.indexOf('MSIE ') > -1 || window.navigator.userAgent.indexOf('Trident/') > -1;

@NgModule({
  declarations: [
    AppComponent,
    DmnEditorComponent,
    DmnTabsWrapperDirective,
    DmnTabComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MsalModule.forRoot({
      auth: {
        clientId: '6792e6cd-02b0-4176-967d-7d0a9ee7921d',
        authority: 'https://login.microsoftonline.com/52217199-75dd-4e06-8891-77a8eb5eee12/', // This is your tenant ID
        redirectUri: 'http://localhost:8080',
        postLogoutRedirectUri: 'http://localhost:8080'
      },
      cache: {
        cacheLocation: 'localStorage',
        storeAuthStateInCookie: isIE, // Set to true for Internet Explorer 11
      },
    }, {
      popUp: !isIE,
      consentScopes: [
        'openid',
        'profile',
        '6792e6cd-02b0-4176-967d-7d0a9ee7921d/.default'
      ],
      protectedResourceMap: [
        ['https://atlisdv.igatlis.com/openid/userinfo', ['openid', 'email', '6792e6cd-02b0-4176-967d-7d0a9ee7921d/.default']]

      ],
      extraQueryParameters: {}
    })

  ],
  providers: [HttpClient, {
    provide: HTTP_INTERCEPTORS,
    useClass: MsalInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
